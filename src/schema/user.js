const { gql } = require('apollo-server-express');
const userSchema = gql`
type Query {
    user(id: ID!): User
}
type User {
    id: ID!
        name: String!
        email: String!
        password: String!
}
type Mutation {
    login(email: String!, password: String!): String
    register(name: String!, email: String!, password: String!): User
}`
;
module.exports = userSchema;