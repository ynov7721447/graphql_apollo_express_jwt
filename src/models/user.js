const db = require('../db/database');

class User {
    constructor({ id, name, email, password }) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    static async findById(id) {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM users WHERE id = ?', [id], (err, row) => {
                if (err) reject(err);
                resolve(row ? new User(row) : null);
            });
        });
    }

    static async findOne({ email }) {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM users WHERE email = ?', [email], (err, row) => {
                if (err) reject(err);
                resolve(row ? new User(row) : null);
            });
        });
    }

    async save() {
        return new Promise((resolve, reject) => {
            const { name, email, password } = this;
            db.run('INSERT INTO users (name, email, password) VALUES (?, ?, ?)',
                [name, email, password],
                function(err) {
                    if (err) {
                        if (err.errno === 19 && err.code === 'SQLITE_CONSTRAINT') {
                            reject(new Error('User already exists'));
                        } else {
                            reject(err);
                        }
                    } else {
                        resolve(new User({
                            id: this.lastID,
                            name,
                            email,
                            password
                        }));
                    }
                }
            );
        });
    }
}

module.exports = User;
