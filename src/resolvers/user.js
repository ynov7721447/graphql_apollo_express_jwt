const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const resolvers = {
    Query: {
        user: async (_, { id }, context) => {
            if (!context.user) throw new Error('Not authenticated');
            return await User.findById(id);
        },
    },
    Mutation: {
        login: async (_, { email, password }) => {
            const user = await User.findOne({ email });
            if (!user) throw new Error('User not found');

            const isValid = await bcrypt.compare(password, user.password);
            if (!isValid) throw new Error('Invalid password');

            const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, { expiresIn: '1h' });
            return token;
        },
        register: async (_, { name, email, password }) => {
            try {
                console.log('Registering user:', name, email);

                const existingUser = await User.findOne({ email });
                if (existingUser) {
                    throw new Error('User with this email already exists');
                }

                const hashedPassword = await bcrypt.hash(password, 10);
                const newUser = new User({ name, email, password: hashedPassword });
                console.log('New user object:', newUser);

                const savedUser = await newUser.save();
                console.log('Saved user:', savedUser);

                return savedUser;
            } catch (error) {
                if (error.message === 'User already exists') {
                    throw new Error('User with this email already exists');
                }
                console.error('Error creating user:', error);
                throw new Error('Error creating user: ' + error.message);
            }
        },
    },
};

module.exports = resolvers;
