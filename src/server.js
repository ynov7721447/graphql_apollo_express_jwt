require('dotenv').config();
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const userSchema = require('./schema/user');
const userResolvers = require('./resolvers/user');
const auth = require('./middleware/auth');

const app = express();

const server = new ApolloServer({
    typeDefs: userSchema,
    resolvers: userResolvers,
    context: ({ req }) => {
        const user = auth(req);
        return { user };
    },
});

async function startServer() {
    await server.start();
    server.applyMiddleware({ app });

    const PORT = process.env.PORT || 4000;
    app.listen(PORT, () => {
        console.log(`Server running on http://localhost:${PORT}${server.graphqlPath}`);
    });
}

startServer();